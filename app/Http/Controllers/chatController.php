<?php

namespace App\Http\Controllers;

use App\Mensajes;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class chatController extends Controller
{
    function index(){

        return view('chatview');
    }

    function mensajes(){


        $mensajes = DB::table('mensaje')
            ->join('users', 'mensaje.users_id', '=', 'users.id')
            ->select('mensaje.*', 'users.name as nombre')
            ->oldest()
            ->get();

        return view('request_mensajes', compact('mensajes'));
    }
    function usuarios(){
        $usuarios = User::all();
        return view('request_usuarios', compact('usuarios'));
    }

    function send_mensaje(Request $request){

        Mensajes::create([
            'mensaje' => $request->mensaje,
            'users_id' => Auth::user()->id,

        ]);
    }
}
