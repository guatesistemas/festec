<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mensajes extends Model
{
    protected $table= 'mensaje';


    protected $fillable = [
        'id_mensaje', 'mensaje', 'users_id'
    ];

    protected $primaryKey = 'id_mensaje';
}
