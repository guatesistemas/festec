
@foreach($mensajes as $mensaje)

        <div class="incoming_msg">
            <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
            <div class="received_msg">
                <div class="received_withd_msg">
                    <p style="font-weight: bold;">{{$mensaje->mensaje}}</p>
                    <span class="time_date">{{$mensaje->created_at}} - {{$mensaje->nombre}}</span></div>
            </div>
        </div>
    </div>
@endforeach
