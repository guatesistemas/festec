<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/chat', 'chatController@index')->name('index');
Route::post('/mensaje_request', 'chatController@mensajes')->name('mensaje_request');
Route::post('/mensaje_send', 'chatController@send_mensaje')->name('mensaje_send');
Route::post('/usuario_request', 'chatController@usuarios')->name('usuario_request');
